package JavaProgram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.testng.annotations.Test;

public class MaxNMin {
	
	 //7) Find the maximum and minimum value in a given int array: {11,13,3,7,18,8,9}
	static int [] in = {11,13,3,7,18,8,9};
	
	/*public static void main(String[] args) {
		
		
		getMethod1();
		getMethod2();	
		
	}
*/		@Test
		public static void getMethod1() {
		
			List<Integer> list = new ArrayList<>();
			for(Integer eachin:in) {
				list.add(eachin);
			}
			Collections.sort(list);
		System.out.println("Min Value:" +list.get(0));
		System.out.println("Max Value:" +list.get(list.size()-1));	
		
		}
		
		@Test
		public static void getMethod2() {
		Arrays.sort(in);
		
		System.out.println("Min value:" +in[0]);
		System.out.println("Max value:" +in[in.length-1]);
		
			
		}
		

	}

